const express = require("express");

// The "taskController" allow us to use the function defined inside it.
const taskController = require("../controller/taskController");

//Allows access HTTP Method middlewares that makes it easier to create routes for our application.
const router = express.Router();



//Route to get all tasks
router.get("/", (req, res)=>{
										//task								//task	
	taskController.getAllTasks().then (resultFromController => res.send(resultFromController));
})

// Route to Create a Task
//localhost:3001/task it is the same with localhost:3001/task/
router.post("/", (req, res)=>{
	// The "createTask" function needs data from the request body, so we need it to supply in the taskController.createTask(argument).
	taskController.createTask(req.body).then(resultFromController => res.send(
		resultFromController));
})


// Route to delete a task
// colon (:) is an identifier that helps create a dynamic route which allow us to supply information in the url.
// ":id" is a wildcard where you can put the objectID as the value.
// Ex: localhost:3001/tasks/:id or localhost:3000/tasks/123456

router.delete("/:id", (req, res)=>{
	//If information will be coming from the URl, the data can be accessed from the request "params" property.
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.patch("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

 // Route to get specific task
router.get("/:id", (req, res)=>{
   taskController.getSpecificTasks(req.params.id).then(resultFromController => res.send(resultFromController));
})

//Route update a specific task
router.put("/:id/complete", (req, res)=>{
   taskController.updateTaskStatus(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Use "modules.exports" to export the router object to be use in the server.
module.exports = router;