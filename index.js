// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

//This allow us to use all the routes defined in the "taskRoutes.js"
const taskRoute = require("./routes/taskRoute");

//Server setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Mongodb connection
mongoose.connect("mongodb+srv://admin:admin@cluster0.scuimpl.mongodb.net/b183_to-do?retryWrites=true&w=majority",{useNewUrlParser: true, useUnifiedTopology: true});

// Set the notification for connection or failure
let db = mongoose.connection;

//Notify on error
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "tasks" route.
//localhost:3001/tasks
app.use("/tasks", taskRoute);

//Server listen
app.listen(port, () => console.log(`Now listening to port ${port}`));


/*
Separation of concerns

Model Folder
- Contains the object Schemas and defines the object structure and content.

Controllers Folder
- Contain the function and business logic of our JS application
- Meaning all the operation it can do will be placed here.

Routes Folder
- Contain all the endpoints and assign the http methods for our application.
app.get("/", )
- We separate the routes such that the server/"index.js" only contain information on the server.

JS modules
require => to include a specific module/package.
export.module => to treat a value as a "package" that can be used by other files.
*/


/*
Instructions s31 Activity:
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.
*/
